# FPCloud

Proyecto DAW

Crear base de datos:
    1. Ejecutar en el administrador de bases de datos fpcloud.sql.
    2. Importar ListadoCentros.csv (puede dar error por el campo AUTO_INCREMENT pero igualmente se insertan los datos, comprobar si los campos se han creado).
    3. Importar ListadoCiclos.csv (puede dar error por el campo AUTO_INCREMENT pero igualmente se insertan los datos, comprobar si los campos se han creado).
    4. Importar centrosfp_ciclosfp.csv.
    5. En la tabla asignaturas ejecutar insert.sql.

Parámetros de conexion a la BD

$mysql_server="localhost";		// servidor donde se encuentra la BD
$mysql_login="marta";			// usuario MySQL que utilizo en la conexion
$mysql_pass="admin";			// passwd del usuario en MySQL
$mysql_bbdd="id13422188_fpcloud";			// BBDD

NOTA: El login y pass se pueden modificar en el archivo pdo.inc.php

Crear primer usuario administrador:

    Para crear el primer usuario administrador, registrar el usuario mediante el formulario de registro, acceder al administrador de la base de datos, tabla usuarios y modificar el campo 'rol' de 'User' a 'Administrator', guardar cambios.

    Una vez que tengamos un usuario administrador ya podemos desde la aplicación gestionar los siguientes usuarios.

Control de versiones:

    Si se desea clonar, descargar o ver la trayectoria del proyecto, se puede hacer desde el repositorio https://gitlab.com/marta_panizo/fpcloud.