<?php 
    session_start(); 
    ob_start();
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>FP·Cloud</title>

    <link  rel="icon" href="images/favicon.png" type="image/png" />
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">

    <script src="https://kit.fontawesome.com/4d8b99f286.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="js/login.js"></script>
</head>
    <?php
        include('pdo.inc.php');
        include('s.inc.php');

        if (!isset($_POST['email']) || !isset($_POST['password'])) {
          header('Location: index.php'); 
          exit;        
        } elseif (empty($_POST['email']) || empty($_POST['password'])) {
          header('Location: index.php'); 
          exit;        
        } else {
            $email=$_POST['email'];
            $password=crypt($_POST['password'],$salt);

            conecta();
            $sql="SELECT id_usuario, contrasena, email, nombre_usuario, apellidos_usuario, id_ciclo, id_centro, rol FROM usuarios WHERE email='$email' and contrasena='$password'";
            $resultado = $c->query($sql);
            if ($resultado->rowCount()==0) {
                header('Location: index.php');
                exit;
            } else {
                while ($registro=$resultado->fetch()) {
                  $nombre=$registro['nombre_usuario'];
                  $apellidos=$registro['apellidos_usuario'];
                  $idCiclo=$registro['id_ciclo'];
                  $idCentro=$registro['id_centro'];
                  $idUsuario=$registro['id_usuario'];
                  $rol=$registro['rol'];
                }
                $_SESSION['logueado']=true;
                $_SESSION['login']=$email;
                $_SESSION['nombre']=$nombre;
                $_SESSION['apellidos']=$apellidos;
                $_SESSION['idCiclo']=$idCiclo;
                $_SESSION['idCentro']=$idCentro;
                $_SESSION['idUsuario']=$idUsuario;
                $_SESSION['rol']=$rol;

                header('Location: menu.php');
                exit;
            }
              //cerramos la conexion
              $c=null;
          } 
    ?>
  </body>
</html>