<?php session_start(); ?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>FP·Cloud</title>

    <link  rel="icon" href="images/favicon.png" type="image/png" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css">

    <script src="https://kit.fontawesome.com/4d8b99f286.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/menu.js"></script>
</head>
  <body>
    <div class="container-fluid container-contenido container-menu">
        <div id="header-menu" class="navbar navbar-light bg-ligh">
            <span class="ham">
                <a href="#" id="boton-ham"><i class="fas fa-bars fa-2x"></i></a>
            </span>          
        </div>    
        <div id="vertical-nav">
            <div class="titulo-menu mb-3">
                <img src="images/logos/logofpcloud1.png" alt="Logotipo FP·Cloud" title="Logotipo FP·Cloud">
            </div> 
            <div class="lista-menu">
                <ul>
                    <li><a href="menu.php">Inicio</a></li>  
                    <li><a href="todos.php">Archivos</a></li>    
                    <li><a href="asignaturas.php">Asignaturas</a></li>
                    <li><a href="privado.php">Documentos</a></li>  
                    <li><a href="nuevo.php">Nuevo documento</a></li>                                  
                </ul>
            </div>                 
        </div>        
        <div id="contenido-menu" class="contenido-menu1">
            <header class="row justify-content-between header-contenido-menu">
                <div class="menu-archivos">
                    <a class="menu-archivos-item enlace-desactivado" href="subir.php"><i class="fas fa-cloud-upload-alt fa-2x"></i></a>
                </div>
                <div class="row">
                    <form class="form-inline flex-nowrap buscar" action="searchdoc.php" method="POST">
                        <input class="form-control form-control-sm mr-3 input-buscador" type="text" placeholder="Buscar" aria-label="Search" name="buscar">
                        <button type="submit" class="btn btn-link buscador" alt="Buscar" title="Buscar"><i class="fas fa-search" aria-hidden="true"></i></button>
                    </form>
                    <a href="#"><div class="row align-items-center justify-content-center flex-nowrap menu-perfil">
                    <div class="saludo-perfil flex-column text-center">
                            <div>
                                <?php
                                    $nombre=$_SESSION['nombre'];
                                    $apellidos=$_SESSION['apellidos'];
                                    $rol=$_SESSION['rol'];
                                    if ($rol=='User') {
                                        echo '<div class="dropdown show">';                          
                                        echo '<a href="#" class="dropdown-toggle eliminar-flecha nombre-perfil" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hola '.$nombre.'<i class="fas fa-angle-down padding-icon"></i></a>';
                                        echo '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">';                                     
                                        echo '<a class="dropdown-item nonpadding-side" href="salir.php">Cerrar sesión</a>';
                                        echo '</div></div>';
                                    } else {
                                        echo '<div class="dropdown show">';                          
                                        echo '<a href="#" class="dropdown-toggle eliminar-flecha nombre-perfil>" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hola '.$nombre.'<i class="fas fa-angle-down padding-icon"></i></a>';
                                        echo '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">';
                                        echo '<a class="dropdown-item nonpadding-side" href="administrar.php">Configuración</a>';
                                        echo '<div class="dropdown-divider"></div>';                                      
                                        echo '<a class="dropdown-item nonpadding-side" href="salir.php">Cerrar sesión</a>';
                                        echo '</div></div>';
                                    }                                   
                                ?>
                            </div>
                            <a class="anadir-foto" data-toggle="modal" data-target="#exampleModalCenter" href="">Añadir foto</a>
                        </div>
                        <div class="img-perfil">
                            <?php
                                $email=$_SESSION['login'];
                                include('pdo.inc.php');
                                conecta();
                                $sql="SELECT imgperfil FROM usuarios WHERE email='$email'";
                                $resultado = $c->query($sql);
                                while ($registro=$resultado->fetch()) {
                                    $imgperfil=$registro['imgperfil'];
                                    if ($imgperfil=='') {
                                        echo '<img src="images/icons/personas.png"/>';
                                    } else {
                                        echo '<img src="'.$imgperfil.'"/>';
                                    }
                                }                              
                            ?>
                        </div>              
                    </div></a>
                </div>
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Añadir imagen de perfil</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="subirImgPerfil.php" method="post" enctype="multipart/form-data">
                            <div class="modal-body">                  
                                <div class="custom-file">           
                                    <input type="file" class="custom-file-input" id="customFile" name="archivo">
                                    <label class="custom-file-label" for="customFile" data-browse="Elegir">Elegir archivo</label>                              
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary">Guardar cambios</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </header>
            <div class="central row flex-nowrap justify-content-center">
                <div class="contenido-central-menu row flex-wrap justify-content-start">
                    <div class="col-12 ultimos-title">
                        <h5>Editor de documentos</h5>
                    </div>
                    <div class="col-12 table-responsive-md tabla-menu ultimos-content">
                        <form action="guardarEditDoc.php" method="post">
                            <div class="form-group row">
                                <?php 
                                    $nombreDoc=$_GET['fichero'];
                                    $sql="SELECT contenido, nombre_documento FROM documentos WHERE nombre_documento LIKE '$nombreDoc'";
                                    $resultado = $c->query($sql);
                                    while ($registro=$resultado->fetch()) {
                                        $contenido=$registro['contenido'];
                                    }
                                ?>
                                <label for="nombreDoc">Nombre documento</label>
                                <input class="form-control" name="nombreDoc" type="text" value="<?php echo $nombreDoc ?>" readonly/>
                            </div>
                            <textarea id="summernote" name="editordata"><?php echo $contenido ?></textarea>                           
                            <button type="submit" class="btn btn-primary mt-4">Guardar</button>
                        </form>
                    </div>
                </div>
                <header class="row justify-content-end header-contenido-menu2">
                    <div class="dropdown show">
                        <a href="#" class="dropdown-toggle eliminar-flecha" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-h fa-2x"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item nonpadding-side" href="#">
                            <div class="row align-items-center justify-content-center flex-nowrap menu-perfil">
                                <div class="saludo-perfil">
                                    <?php                        
                                        echo '<span class="nombre-perfil">Hola '.$nombre.'</span>';
                                    ?>
                                </div>
                                <div class="img-perfil">
                                    <?php
                                        conecta();
                                        $sql="SELECT imgperfil FROM usuarios WHERE email='$email'";
                                        $resultado = $c->query($sql);
                                        while ($registro=$resultado->fetch()) {
                                            $imgperfil=$registro['imgperfil'];
                                            if ($imgperfil==NULL) {
                                                echo '<img src="images/icons/personas.png"/>';
                                            } else {
                                                echo '<img src="'.$imgperfil.'"/>';
                                            }
                                        }                               
                                        //cerramos la conexion
                                        $c=null;
                                    ?>
                                </div>              
                            </div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item nonpadding-side" href="#">
                            <form class="form-inline flex-nowrap buscar" action="search.php" method="POST">
                                <input class="form-control form-control-sm mr-3 input-buscador" type="text" placeholder="Buscar" aria-label="Search" name="buscar">
                                <button type="submit" class="btn btn-link buscador"><i class="fas fa-search" aria-hidden="true"></i></button>
                            </form>
                        </a>
                        <div class="dropdown-divider"></div>
                        <?php 
                            if ($rol=='Administrator') {
                                echo '<a class="dropdown-item nonpadding-side" href="administrar.php">Configuración</a>';                               
                            }
                        ?>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item nonpadding-side" href="salir.php">
                            Cerrar sesión
                        </a>
                    </div>
                </header>
            </div>
        </div>        
    </div>   
    <div class="row justify-content-center pt-3 pb-3 fondo-color-footer">                
        <div class="d-flex justify-content-center flex-wrap col-lg-7 col-md-7 col-sm-8 col-xs-12 legal-footer pt-3">               
            <span class="border-right border-white pr-3">Desarrollado por Marta Panizo</span><a href="#" class="border-right border-white pl-3 pr-3" data-toggle="modal" data-target="#exampleModal4" data-whatever="@contactoFPCloud">Contacto FPCloud</a><a href="#" class="border-right border-white pl-3 pr-3" data-toggle="modal" data-target="#exampleModalLong">Términos y condiciones</a><a href="#" class="pl-3" data-toggle="modal" data-target="#exampleModalLong2">Política de privacidad</a>
        </div>
    </div>
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Términos y condiciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <h5>INFORMACIÓN RELEVANTE</h5>
                <p>Es requisito necesario para la adquisición de los productos que se ofrecen en este sitio, que lea y acepte los siguientes Términos y Condiciones que a continuación se redactan. El uso de nuestros servicios así como la compra de nuestros productos implicará que usted ha leído y aceptado los Términos y Condiciones de Uso en el presente documento. Todas los productos  que son ofrecidos por nuestro sitio web pudieran ser creadas, cobradas, enviadas o presentadas por una página web tercera y en tal caso estarían sujetas a sus propios Términos y Condiciones. En algunos casos, para adquirir un producto, será necesario el registro por parte del usuario, con ingreso de datos personales fidedignos y definición de una contraseña.</p>
                <p>El usuario puede elegir y cambiar la clave para su acceso de administración de la cuenta en cualquier momento, en caso de que se haya registrado y que sea necesario para la compra de alguno de nuestros productos.  no asume la responsabilidad en caso de que entregue dicha clave a terceros.</p>
                <p>Todas las compras y transacciones que se lleven a cabo por medio de este sitio web, están sujetas a un proceso de confirmación y verificación, el cual podría incluir la verificación del stock y disponibilidad de producto, validación de la forma de pago, validación de la factura (en caso de existir) y el cumplimiento de las condiciones requeridas por el medio de pago seleccionado. En algunos casos puede que se requiera una verificación por medio de correo electrónico.</p>
                <h5>LICENCIA</h5>  
                <p>A través de su sitio web concede una licencia para que los usuarios utilicen  los productos que son vendidos en este sitio web de acuerdo a los Términos y Condiciones que se describen en este documento.</p>
                <h5>USO NO AUTORIZADO</h5>
                <p>En caso de que aplique (para venta de software, templetes, u otro producto de diseño y programación) usted no puede colocar uno de nuestros productos, modificado o sin modificar, en un CD, sitio web o ningún otro medio y ofrecerlos para la redistribución o la reventa de ningún tipo.</p>
                <h5>PROPIEDAD</h5>
                <p>Usted no puede declarar propiedad intelectual o exclusiva a ninguno de nuestros productos, modificado o sin modificar. Todos los productos son propiedad  de los proveedores del contenido. En caso de que no se especifique lo contrario, nuestros productos se proporcionan  sin ningún tipo de garantía, expresa o implícita. En ningún esta compañía será  responsables de ningún daño incluyendo, pero no limitado a, daños directos, indirectos, especiales, fortuitos o consecuentes u otras pérdidas resultantes del uso o de la imposibilidad de utilizar nuestros productos.</p>
                <h5>POLÍTICA DE REEMBOLSO Y GARANTÍA</h5>
                <p>En el caso de productos que sean  mercancías irrevocables no-tangibles, no realizamos reembolsos después de que se envíe el producto, usted tiene la responsabilidad de entender antes de comprarlo.  Le pedimos que lea cuidadosamente antes de comprarlo. Hacemos solamente excepciones con esta regla cuando la descripción no se ajusta al producto. Hay algunos productos que pudieran tener garantía y posibilidad de reembolso pero este será especificado al comprar el producto. En tales casos la garantía solo cubrirá fallas de fábrica y sólo se hará efectiva cuando el producto se haya usado correctamente. La garantía no cubre averías o daños ocasionados por uso indebido. Los términos de la garantía están asociados a fallas de fabricación y funcionamiento en condiciones normales de los productos y sólo se harán efectivos estos términos si el equipo ha sido usado correctamente. Esto incluye:</p>
                <p></p>
                <h5>COMPROBACIÓN ANTIFRAUDE</h5>
                <p>– De acuerdo a las especificaciones técnicas indicadas para cada producto.
                  – En condiciones ambientales acorde con las especificaciones indicadas por el fabricante.
                  – En uso específico para la función con que fue diseñado de fábrica.
                  – En condiciones de operación eléctricas acorde con las especificaciones y tolerancias indicadas.</p>
                <h5>PRIVACIDAD</h5>
                <p>Este sitio web  garantiza que la información personal que usted envía cuenta con la seguridad necesaria. Los datos ingresados por usuario o en el caso de requerir una validación de los pedidos no serán entregados a terceros, salvo que deba ser revelada en cumplimiento a una orden judicial o requerimientos legales.</p>
                <p>La suscripción a boletines de correos electrónicos publicitarios es voluntaria y podría ser seleccionada al momento de crear su cuenta.</p>
                <p>Se reserva los derechos de cambiar o de modificar estos términos sin previo aviso.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div> 
        </div>   
        <div class="modal fade" id="exampleModalLong2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle2" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle2">Política de privacidad</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>A través de este sitio web no se recaban datos de carácter personal de los usuarios sin su conocimiento, ni se ceden a terceros.</p>
                  <p>Con la finalidad de ofrecerle el mejor servicio y con el objeto de facilitar el uso, se analizan el número de páginas visitadas, el número de visitas, así como la actividad de los visitantes y su frecuencia de utilización. A estos efectos, FPCloud utiliza la información estadística elaborada por el Proveedor de Servicios de Internet.</p>
                  <p>FPCloud no utiliza cookies para recoger información de los usuarios, ni registra las direcciones IP de acceso. Únicamente se utilizan cookies propias,  de sesión, con finalidad técnica (aquellas que permiten al usuario la navegación a través del sitio web y la utilización de las diferentes opciones y servicios que en ella existen).</p>
                  <p>El portal del que es titular FPCloud contiene enlaces a sitios web de terceros, cuyas políticas de privacidad son ajenas a la de FPCloud. Al acceder a tales sitios web usted puede decidir si acepta sus políticas de privacidad y de cookies. Con carácter general, si navega por internet usted puede aceptar o rechazar las cookies de terceros desde las opciones de configuración de su navegador.</p>
                  <h5>Información básica sobre protección de datos</h5>
                  <p>A continuación le informamos sobre la política de protección de datos de la Agencia Española de Protección de Datos.</p>
                  <h5>Responsable del tratamiento</h5>
                  <p>Los datos de carácter personal que se pudieran recabar directamente del interesado serán tratados de forma confidencial y quedarán incorporados a la correspondiente actividad de tratamiento titularidad de FPCloud.</p>
                  <p>La relación actualizada de las actividades de tratamiento que FPCloud lleva a cabo se encuentra disponible en el siguiente enlace al registro de actividades de FPCloud.</p>
                  <h5>Finalidad</h5>
                  <p>La finalidad del tratamiento de los datos corresponde a cada una de las actividades de tratamiento que realiza FPCloud y que están accesibles en el registro de actividades de tratamiento.</p>
                  <h5>Legitimación</h5>
                  <p>El tratamiento de sus datos se realiza para el cumplimiento de obligaciones legales por parte de FPCloud, para el cumplimiento de misiones realizada en interés público o en el ejercicio de poderes públicos conferidos a FPCloud, así como cuando la finalidad del tratamiento requiera su consentimiento, que habrá de ser prestado mediante una clara acción afirmativa.</p>
                  <p>Puede consultar la base legal para cada una de las actividades de tratamiento que lleva a cabo FPCloud en  el siguiente enlace al registro de actividades de FPCloud.</p>
                  <h5>Conservación de datos</h5>
                  <p>Los datos personales proporcionados se conservarán durante el tiempo necesario para cumplir con la finalidad para la que se recaban y para determinar las posibles responsabilidades que se pudieran derivar de la finalidad, además de los períodos establecidos en la normativa de archivos y documentación.</p>
                  <h5>Comunicación de datos</h5>
                  <p>Con carácter general no se comunicarán los datos personales a terceros, salvo obligación legal, entre las que pueden estar las comunicaciones al Defensor del Pueblo, Jueces y Tribunales, interesados en los procedimientos relacionados con la reclamaciones presentadas.</p>
                  <p>Puede consultar los destinatarios para cada una de las actividades de tratamiento que lleva a cabo FPCloud en el siguiente  enlace al registro de actividades de FPCloud.</p>
                  <h5>Derechos de los interesados</h5>
                  <p>Cualquier persona tiene derecho a obtener confirmación sobre los tratamientos que de sus datos que se llevan a cabo por FPCloud.</p>
                  <p>Puede ejercer sus derechos de acceso, rectificación, supresión y portabilidad de sus datos, de limitación y oposición a su tratamiento, así como a no ser objeto de decisiones basadas únicamente en el tratamiento automatizado de sus datos, cuando procedan, ante la Agencia Española de Protección de Datos, C/Jorge Juan, 59, 28001- Madrid o en la dirección de correo electrónico dgfs@fcloud.com.</p>
                  <h5>Aviso legal</h5>
                  <p>Este portal, cuyo titular es FPCloud, con NIF Q281678D, domicilio en la calle Jorge Juan, nº59, 28001 Madrid, y teléfono 901 199 999, está constituido por los sitios web asociados a los dominios fpcloud.000webhostapp.com/.</p>
                  <h5>Propiedad intelectual e industrial</h5>
                  <p>El diseño del portal y sus códigos fuente, así como los logos, marcas y demás signos distintivos que aparecen en el mismo pertenecen a FPCloud y están protegidos por los correspondientes derechos de propiedad intelectual e industrial.</p>
                  <h5>Responsabilidad de los contenidos</h5>
                  <p>FPCloud no se hace responsable de la legalidad de otros sitios web de terceros desde los que pueda accederse al portal. FPCloud tampoco responde por la legalidad de otros sitios web de terceros, que pudieran estar vinculados o enlazados desde este portal.</p>
                  <p>FPCloud se reserva el derecho a realizar cambios en el sitio web sin previo aviso, al objeto de mantener actualizada su información, añadiendo, modificando, corrigiendo o eliminando  los contenidos publicados o el diseño del portal.</p>
                  <p>FPCloud no será responsable del uso que terceros hagan de la información publicada en el portal, ni tampoco de los daños sufridos o pérdidas económicas que, de forma directa o indirecta, produzcan o puedan producir perjuicios económicos, materiales o sobre datos, provocados por el uso de dicha información.</p>
                  <h5>Reproducción de contenidos</h5>
                  <p>Se prohíbe la reproducción total o parcial de los contenidos publicados en el portal. No obstante, los contenidos que sean considerados como datos abiertos en la Sede Electrónica, publicados según lo previsto en el Real Decreto 1495/2011, de 24 de octubre, de desarrollo de la Ley 37/2007, sobre reutilización de la información del sector público, para el ámbito del sector público estatal, podrán ser objeto de reproducción en los términos contenidos en el siguiente Aviso.</p>
                  <h5>Portal de transparencia</h5>
                  <p>A través de la información publicada en el portal de transparencia, FPCloud atiende de forma periódica y actualizada el principio de publicidad activa establecido por la Ley 19/2013, de 9 de diciembre, de transparencia, acceso a la información pública y buen gobierno, con los mecanismos adecuados para facilitar la accesibilidad, la interoperabilidad, la calidad y la reutilización de la información, así como su identificación y localización.</p>
                  <h5>Ley aplicable</h5>
                  <p>La ley aplicable en caso de disputa o conflicto de interpretación de los términos que conforman este aviso legal, así como cualquier cuestión relacionada con los servicios del presente portal, será la ley española.</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>  
        <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel4">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Recipient:</label>
                    <input type="text" class="form-control" id="recipient-name" value="@contactoFPCloud">
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="col-form-label">Message:</label>
                    <textarea class="form-control" id="message-text"></textarea>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
              </div>
            </div>
          </div>
        </div>
        <script>
            $(document).ready(function() {
                $('#summernote').summernote();
            });
        </script>
  </body>
</html>
