# Script de creación de base de datos

# Borrar base de datos si existe
DROP DATABASE IF EXISTS id13422188_fpcloud;

# Crear base de datos
CREATE DATABASE id13422188_fpcloud CHARACTER SET utf8 COLLATE utf8_unicode_ci;

# Usar base de datos
USE id13422188_fpcloud;

# Crear tablas
CREATE TABLE centrosfp (
    id_centro INT AUTO_INCREMENT NOT NULL,
    provincia VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
    localidad VARCHAR(80) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
    nombre_centro VARCHAR(80) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,    
    PRIMARY KEY (id_centro)
) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE ciclosfp (
    id_ciclo INT AUTO_INCREMENT NOT NULL,
    grado VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci,
    nombre_ciclo VARCHAR(160) CHARACTER SET utf8 COLLATE utf8_spanish2_ci,
    PRIMARY KEY (id_ciclo)
) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE centrosfp_ciclosfp (
    id_centro INT NOT NULL,
    id_ciclo INT NOT NULL,
    PRIMARY KEY (id_centro, id_ciclo)
) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE usuarios (
    id_usuario INT AUTO_INCREMENT NOT NULL,
    nombre_usuario VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
    apellidos_usuario VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
    email VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
    nick_telegram VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci,
    rol VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
    contrasena VARCHAR(17) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
    imgperfil VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci,
    id_centro INT NOT NULL,
    id_ciclo INT NOT NULL,    
    PRIMARY KEY (id_usuario),
    FOREIGN KEY (id_centro) REFERENCES centrosfp(id_centro),
    FOREIGN KEY (id_ciclo) REFERENCES ciclosfp(id_ciclo)
) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE asignaturas (
    id_asignatura INT AUTO_INCREMENT NOT NULL,
    nombre_asignatura VARCHAR(160) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
    curso VARCHAR(2) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
    id_ciclo INT NOT NULL,
    PRIMARY KEY (id_asignatura),
    FOREIGN KEY (id_ciclo) REFERENCES ciclosfp(id_ciclo)
) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE archivos (
    id_archivo INT AUTO_INCREMENT NOT NULL,
    nombre_archivo VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
    tipo VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci,
    fecha_subida DATE NOT NULL,
    hora_subida TIME NOT NULL,
    id_asignatura INT NOT NULL,
    id_usuario INT NOT NULL,
    PRIMARY KEY (id_archivo),
    FOREIGN KEY (id_asignatura) REFERENCES asignaturas(id_asignatura),
    FOREIGN KEY (id_usuario) REFERENCES usuarios(id_usuario)
) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE documentos (
  id_documento INT AUTO_INCREMENT NOT NULL,
  nombre_documento VARCHAR(160)CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  fecha date NOT NULL,
  hora time NOT NULL,
  contenido TEXT CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  id_usuario INT NOT NULL,
  PRIMARY KEY (id_documento),
  FOREIGN KEY (id_usuario) REFERENCES usuarios(id_usuario)
) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


