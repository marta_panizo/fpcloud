<?php 
    session_start(); 
    ob_start();
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>FP·Cloud</title>

    <link  rel="icon" href="images/favicon.png" type="image/png" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css">

    <script src="https://kit.fontawesome.com/4d8b99f286.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </head>
  <body class="body-ini">
    <div class="container-fluid container-contenido">
        <div class="row justify-content-center align-items-center container-form">
            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12 text-white margin-large-alta">
                <div class="titulo-inicio mb-3 mt-5">
                  <img class="logo" src="images/logos/logofpcloud1.png" alt="Logotipo FP·Cloud" title="Logotipo FP·Cloud">
                </div>
                <div class="border border-light p-4 fondo-color">
                  <div class="row justify-content-center align-items-center">
    <?php
        if (!isset($_POST['nombre']) || !isset($_POST['apellidos']) || !isset($_POST['email']) || !isset($_POST['password']) || !isset($_POST['repassword']) || !isset($_POST['acepto'])) {
          header('Location: registro.php');  
          exit;       
        } elseif (empty($_POST['nombre']) || empty($_POST['apellidos']) || empty($_POST['email']) || empty($_POST['password']) || empty($_POST['repassword']) || empty($_POST['acepto'])) {
          header('Location: registro.php'); 
          exit;        
        } else {
            $nombre=$_POST['nombre'];
            $apellidos=$_POST['apellidos'];
            $email=$_POST['email'];
            $password=$_POST['password'];
            $repassword=$_POST['repassword'];
            $acepto=$_POST['acepto'];
            $canalTelegram="";
            $nick="";
            $rol="User";
            $centro=$_POST['centro'];
            $ciclo=$_POST['ciclo'];
            if (isset($_POST['canalTelegram']) || !empty($_POST['canalTelegram'])) {
              if (!isset($_POST['nick']) || empty($_POST['nick'])) {
                header('Location: registro.php');
                exit;
              } else {
                $canalTelegram=$_POST['canalTelegram'];
                $nick=$_POST['nick'];
              }             
            }
          
            include('pdo.inc.php');
            include('s.inc.php');

            conecta();
            $sql="SELECT id_centro FROM centrosfp WHERE nombre_centro='$centro'";
            $resultado = $c->query($sql);
            while ($registro=$resultado->fetch()) {
              $idCentro=$registro['id_centro'];
            }

            $sql="SELECT id_ciclo FROM ciclosfp WHERE nombre_ciclo='$ciclo'";
            $resultado = $c->query($sql);
            while ($registro=$resultado->fetch()) {
              $idCiclo=$registro['id_ciclo'];
            }

            $sql="SELECT email FROM usuarios WHERE email='$email'";
            $resultado = $c->query($sql);
            if ($resultado->rowCount()>0) {
              echo '<span class="col-12 text-center mb-5">El email '.$email.' ya está registrado.</span>';
              echo '<a class="btn btn-primary" href="registro.php" role="button">Aceptar</a>';  
            } else {
              $saltpassword=crypt($password,$salt);
              //ejecutamos un insert
              $sql="INSERT into usuarios (nombre_usuario, apellidos_usuario, email, nick_telegram, rol, id_centro, id_ciclo, contrasena) values ('$nombre','$apellidos','$email','$nick','$rol','$idCentro','$idCiclo','$saltpassword');";
              $registros=$c->exec($sql);

              if ($registros!=0) {
                $_SESSION['logueado']=true;
                $_SESSION['login']=$email;
                $_SESSION['nombre']=$nombre;
                $_SESSION['apellidos']=$apellidos;
                $_SESSION['idCiclo']=$idCiclo;
                $_SESSION['idCentro']=$idCentro;
                $_SESSION['rol']=$rol;

                // Creación de carpeta personal de usuario
                mkdir("./uploads/$email");
                chmod("./uploads/$email", 0777);
                mkdir("./uploads/$email/imgperfil");
                chmod("./uploads/$email/imgperfil", 0777);

                // Creación del árbol de carpetas para subir los archivos
                if (!file_exists("./uploads/$idCiclo")) {
                  mkdir("./uploads/$idCiclo");
                  chmod("./uploads/$idCiclo", 0777);
                  //Según curso
                  mkdir("./uploads/$idCiclo/Primero");
                  chmod("./uploads/$idCiclo/Primero", 0777);
                  $sql="SELECT nombre_asignatura FROM asignaturas A, ciclosfp C WHERE (C.id_ciclo=A.id_ciclo) AND A.curso=1 AND C.id_ciclo=$idCiclo";
                  $resultado = $c->query($sql);
                  while ($registro=$resultado->fetch()) {
                    $nombreAsignatura=$registro['nombre_asignatura'];
                    mkdir("./uploads/$idCiclo/Primero/$nombreAsignatura");
                    chmod("./uploads/$idCiclo/Primero/$nombreAsignatura", 0777);
                  }
                  mkdir("./uploads/$idCiclo/Segundo");
                  chmod("./uploads/$idCiclo/Primero", 0777);
                  $sql="SELECT nombre_asignatura FROM asignaturas A, ciclosfp C WHERE (C.id_ciclo=A.id_ciclo) AND A.curso=2 AND C.id_ciclo=$idCiclo";
                  $resultado = $c->query($sql);
                  while ($registro=$resultado->fetch()) {
                    $nombreAsignatura=$registro['nombre_asignatura'];
                    mkdir("./uploads/$idCiclo/Segundo/$nombreAsignatura");
                    chmod("./uploads/$idCiclo/Primero", 0777);
                  }
                } 
                echo '<span class="col-12 text-center mb-5">Registro de usuario realizado!!</span>';
                echo '<a class="btn btn-primary" href="menu.php" role="button">Aceptar</a>'; 
              } else {
                echo '<span class="col-12 text-center mb-5">Fallo en el registro, por favor inténtalo de nuevo.</span>';
                echo '<a class="btn btn-primary" href="registro.php" role="button">Aceptar</a>';
              }
              //cerramos la conexion
              $c=null;
          }
        } 
    ?>
            </div>
          </div>
        </div>
        <div class="d-flex justify-content-center flex-wrap col-lg-7 col-md-7 col-sm-8 col-xs-12 text-white legal-footer border-top border-white pt-3">               
            <span class="border-right border-white pr-3">Desarrollado por Marta Panizo</span><a href="" class="border-right border-white pl-3 pr-3">Contacto</a><a href="" class="border-right border-white pl-3 pr-3">Términos y condiciones</a><a href="" class="pl-3">Política de privacidad</a>
        </div> 
      </div>
    </div>
  </body>
</html>