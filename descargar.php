<?php

	session_start();
	ob_start();
	// si el usuario no hizo login, le mando fuera
	if(!isset($_SESSION['login'])){
		header("location: index.html");
	}

	if(!isset($_GET['fichero'])){
		header("location: menu.php");
	}

	// compruebo que el fichero a descargar existe
	if(file_exists($_GET['fichero'])){
		$fichero=$_GET['fichero'];

		// creo la cabecera HTTP para que automaticamente descarge el fichero indicado
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($fichero));
		header ("Content-Length: ".filesize($fichero));
		readfile($fichero);
	}
	else{
		header("location: verasignatura.php");
	}

?>
