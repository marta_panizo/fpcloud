window.addEventListener('load',inicio,false);

      function inicio() {
        document.getElementById('canalTelegram').addEventListener('change',agregarEliminar,false);
        document.getElementById('repassword').addEventListener('change',comparar,false);
        document.getElementById('password').addEventListener('change',comparar,false);
      }

      function agregarEliminar() { 
        if (document.getElementById('canalTelegram').checked==true) {           
            var label=document.createElement("label");           
            label.setAttribute("for", "nick");
            label.setAttribute("id", "labelNick");  
            var texto=document.createTextNode("Nick de Telegram");        
            label.appendChild(texto);
            document.getElementById('add').appendChild(label); 
            var input=document.createElement("input");          
            input.setAttribute("type", "text");
            input.setAttribute("name", "nick");
            input.setAttribute("class", "form-control");
            input.setAttribute("required", "required");
            input.setAttribute("pattern","^[@]\\S{1,20}$");
            input.setAttribute("id", "inputNick");
            var texto=document.createTextNode("");        
            input.appendChild(texto);
            document.getElementById('add').appendChild(input);
        } else { 
            var padre=document.getElementById('add');
            while (padre.firstChild) {
                padre.removeChild(padre.firstChild);
            }        
        }
      }

      function comparar() {
          var password=document.getElementById('password');
          var repassword=document.getElementById('repassword');
          if (repassword.value!=password.value || password.value!=repassword.value) {
            repassword.setCustomValidity("Las contraseñas no son iguales.");
          } else {
            repassword.setCustomValidity(""); 
          }         
      }
